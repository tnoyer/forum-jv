<?php

session_start();

$titre="Voir un sujet";

include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//On récupère la valeur de t
$topic = (int) $_GET['t'];

//A partir d'ici, on va compter le nombre de messages pour n'afficher que les 15 premiers
$query=$db->prepare('SELECT topic_titre, topic_post, forum_topic.forum_id, topic_last_post,
  forum_name, auth_view, auth_topic, auth_post
  FROM forum_topic
  LEFT JOIN forum_forum ON forum_topic.forum_id = forum_forum.forum_id
  WHERE topic_id = :topic');
  $query->bindValue(':topic',$topic,PDO::PARAM_INT);
  $query->execute();

  $data=$query->fetch();

  $forum=$data['forum_id'];

  $totalDesMessages = $data['topic_post'] + 1;
  $nombreDeMessagesParPage = 15;
  $nombreDePages = ceil($totalDesMessages / $nombreDeMessagesParPage);

  //fil d'ariane
  echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/>
  <a href="./voirforum.php?f='.$forum.'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a>
  <img src="../images/flecherouge.png" alt="fleche"/> <a href="./voirtopic.php?t='.$topic.'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a>';

  //titre
  echo '<h1>'.$data['topic_titre'].'</h1>';

  //Nombre de pages
  $page = (isset($_GET['page']))?intval($_GET['page']):1;

  $premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;

  //On affiche l'image répondre
  echo'<a href="./poster.php?action=repondre&amp;t='.$topic.'">
  <img src="../images/repondre.gif" alt="Répondre" title="Répondre à ce topic" /></a>';

  //On affiche l'image nouveau topic
  echo'<a href="./poster.php?action=nouveautopic&amp;f='.$data['forum_id'].'">
  <img src="../images/nouveau.gif" alt="Nouveau topic" title="Poster un nouveau topic" /></a>';
  $query->CloseCursor();

  //Enfin on commence la boucle pour afficher les messages
  $query=$db->prepare('SELECT post_id , post_createur , post_texte , post_time ,
    membre_id, membre_pseudo, membre_inscrit, membre_avatar, membre_localisation, membre_post, membre_signature
    FROM forum_post
    LEFT JOIN forum_membres ON forum_membres.membre_id = forum_post.post_createur
    WHERE topic_id =:topic
    ORDER BY post_id
    LIMIT :premier, :nombre');
    $query->bindValue(':topic',$topic,PDO::PARAM_INT);
    $query->bindValue(':premier',(int) $premierMessageAafficher,PDO::PARAM_INT);
    $query->bindValue(':nombre',(int) $nombreDeMessagesParPage,PDO::PARAM_INT);
    $query->execute();

    //On vérifie que la requête a bien retourné des messages
    if ($query->rowCount()<1)
    {
      echo'<p>Il n\'y a aucun post sur ce topic, vérifiez l\'url et réessayez.</p>';
    }
    else
    {
      //Si tout roule on affiche notre tableau puis on remplit avec une boucle
      ?>
      <table class="table-topic">
        <tr>
          <th class="vt-auteur"><strong>Auteurs</strong></th>
          <th class="vt-mess"><strong>Messages</strong></th>
        </tr>
        <?php
        while ($data = $query->fetch())
        {
          //On vérifie les droits du membre
          echo'<tr>
          <td><strong><a href="../profil/voirprofil.php?m='.$data['membre_id'].'&amp;action=consulter">
          '.stripslashes(htmlspecialchars($data['membre_pseudo'])).'</a></strong></td>';

          //Si on est l'auteur du message, on affiche des liens pour le supprimer ou le modifier
          if ($id == $data['post_createur'])
          {
            echo'<td id=p_'.$data['post_id'].'>Posté à '.date('H:i \l\e d M Y',$data['post_time']).'
            <a href="./poster.php?p='.$data['post_id'].'&amp;action=delete">
            <img src="../images/supprimer.gif" alt="Supprimer" title="Supprimer ce message" /></a>
            <a href="./poster.php?p='.$data['post_id'].'&amp;action=edit">
            <img src="../images/editer.gif" alt="Editer" title="Editer ce message" /></a></td>
            </tr>';
          }
          else
          {
            echo'<td>Posté à '.date('H\hi \l\e d M y',$data['post_time']).'</td>
            </tr>';
          }

          //Détails sur le membre qui a posté
          echo'<tr>
          <td>
          <img src="../images/avatars/'.$data['membre_avatar'].'" alt="avatar" /><br />
          <p>Inscription : '.date('d M Y', $data['membre_inscrit']).'</p>
          <p>Messages : '.$data['membre_post'].'</p>
          <p>Localisation : '.stripslashes(htmlspecialchars($data['membre_localisation'])).'</p>
          </td>';

          //Message traduit grace à la fonction code
          echo'<td>'.code(nl2br(stripslashes(htmlspecialchars($data['post_texte'])))).'<br />
          <hr />'.code(nl2br(stripslashes(htmlspecialchars($data['membre_signature'])))).'</td>
          </tr>';
        }
        $query->CloseCursor();

        ?>
      </table>

      <?php

      //on affiche chaque numéro de page de 1 au nombre max de pages
      echo '<p>Page : ';
      for ($i = 1 ; $i <= $nombreDePages ; $i++)
      {
        if ($i == $page) //On affiche pas la page actuelle en lien
        {
          echo $i;
        }
        else
        {
          echo '<a href="voirtopic.php?t='.$topic.'&amp;page='.$i.'">
          ' . $i . '</a> ';
        }
      }
      echo'</p>';

      //On ajoute 1 au nombre de visites de ce topic
      $query=$db->prepare('UPDATE forum_topic
        SET topic_vu = topic_vu + 1 WHERE topic_id = :topic');
        $query->bindValue(':topic',$topic,PDO::PARAM_INT);
        $query->execute();
        $query->CloseCursor();

      } //Fin du if qui vérifiait si le topic contenait au moins un message
      ?>

    </div>
  </div>
</main>
</body>
</html>
