<?php

session_start();

$titre="Voir un forum";

include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//On récupère la valeur de f
$forum = (int) $_GET['f'];

//A partir d'ici, on va compter le nombre de topics pour n'afficher que les 25 premiers
$query=$db->prepare('SELECT forum_name, forum_topic, auth_view, auth_topic
  FROM forum_forum
  WHERE forum_id = :forum');
  $query->bindValue(':forum',$forum,PDO::PARAM_INT);
  $query->execute();
  $data=$query->fetch();

  $totalDesTopics = $data['forum_topic'] + 1;
  $nombreDeTopicsParPage = 25;
  $nombreDePages = ceil($totalDesTopics / $nombreDeTopicsParPage);

  //fil d'ariane
  echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/>
  <a href="../forum/voirforum.php?f='.$forum.'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a>';

  //Le titre du forum
  echo '<h1>'.stripslashes(htmlspecialchars($data['forum_name'])).'</h1>';

  //Nombre de pages
  $page = (isset($_GET['page']))?intval($_GET['page']):1;

  $premierTopicAafficher = ($page - 1) * $nombreDeTopicsParPage;

  //Et le bouton pour poster
  echo'<a href="./poster.php?action=nouveautopic&amp;f='.$forum.'">
  <img src="../images/nouveau.gif" alt="Nouveau topic" title="Poster un nouveau topic" /></a>';

  $query->CloseCursor();

  //On prend tout ce qu'on a sur les topics du forum
  $query=$db->prepare('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_vu, topic_post, topic_time, topic_last_post,
    Mb.membre_pseudo AS membre_pseudo_createur, post_createur, post_time, Ma.membre_pseudo AS membre_pseudo_last_posteur, post_id
    FROM forum_topic
    LEFT JOIN forum_membres Mb ON Mb.membre_id = forum_topic.topic_createur
    LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
    LEFT JOIN forum_membres Ma ON Ma.membre_id = forum_post.post_createur
    WHERE topic_genre = "Annonce" AND forum_topic.forum_id = :forum
    ORDER BY topic_last_post DESC');
    $query->bindValue(':forum',$forum,PDO::PARAM_INT);
    $query->execute();

    //On lance notre tableau seulement s'il y a des requêtes !
    if ($query->rowCount()>0)
    {
      ?>
      <table>
        <tr>
          <th></th>
          <th class="titre"><strong>Annonces</strong></th>
          <th class="nombremessages"><strong>Réponses</strong></th>
          <th class="nombrevu"><strong>Vus</strong></th>
          <th class="auteur"><strong>Auteur</strong></th>
          <th class="derniermessage"><strong>Dernier message</strong></th>
        </tr>

        <?php

        while ($data=$query->fetch())
        {
          //Pour chaque topic :
          //Si le topic est une annonce on l'affiche en haut
          echo'<tr>
          <td><img src="../images/40793.gif" alt="Annonce" /></td>
          <td id="titre"><strong><a href="./voirtopic.php?t='.$data['topic_id'].'" title="Topic commencé à'.date('H\hi \l\e d M,y',$data['topic_time']).'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a></strong></td>
          <td class="nombremessages">'.$data['topic_post'].'</td>
          <td class="nombrevu">'.$data['topic_vu'].'</td>
          <td><a href="../profil/voirprofil.php?m='.$data['topic_createur'].'&amp;action=consulter">'
          .stripslashes(htmlspecialchars($data['membre_pseudo_createur'])).'</a></td>';

          //Selection dernier message
          $nombreDeMessagesParPage = 15;
          $nbr_post = $data['topic_post'] +1;
          $page = ceil($nbr_post / $nombreDeMessagesParPage);

          echo '<td class="derniermessage">
          Par <a href="../profil/voirprofil.php?m='.$data['post_createur'].'&amp;action=consulter">' .stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])).'</a><br />
          A '.date('H\hi \l\e d M y',$data['post_time']).'</a></td>
          </tr>';
        }
        ?>

      </table>

      <?php
    }
    $query->CloseCursor();

    //On prend tout ce qu'on a sur les topics normaux du forum
    $query=$db->prepare('SELECT forum_topic.topic_id, topic_titre, topic_createur, topic_vu, topic_post, topic_time, topic_last_post,
      Mb.membre_pseudo AS membre_pseudo_createur, post_id, post_createur, post_time, Ma.membre_pseudo AS membre_pseudo_last_posteur
      FROM forum_topic
      LEFT JOIN forum_membres Mb ON Mb.membre_id = forum_topic.topic_createur
      LEFT JOIN forum_post ON forum_topic.topic_last_post = forum_post.post_id
      LEFT JOIN forum_membres Ma ON Ma.membre_id = forum_post.post_createur
      WHERE topic_genre <> "Annonce" AND forum_topic.forum_id = :forum
      ORDER BY topic_last_post DESC
      LIMIT :premier ,:nombre');
      $query->bindValue(':forum',$forum,PDO::PARAM_INT);
      $query->bindValue(':premier',(int) $premierTopicAafficher,PDO::PARAM_INT);
      $query->bindValue(':nombre',(int) $nombreDeTopicsParPage,PDO::PARAM_INT);
      $query->execute();

      if ($query->rowCount()>0)
      {
        ?>

        <table>
          <tr>
            <th></th>
            <th class="titre"><strong>Sujets</strong></th>
            <th class="nombremessages"><strong>Réponses</strong></th>
            <th class="nombrevu"><strong>Vus</strong></th>
            <th class="auteur"><strong>Auteur</strong></th>
            <th class="derniermessage"><strong>Dernier message  </strong></th>
          </tr>

          <?php

          while ($data = $query->fetch())
          {
            echo'<tr>
            <td><img src="../images/40793.gif" alt="Message" /></td>
            <td class="titre"><strong><a href="./voirtopic.php?t='.$data['topic_id'].'"title="Topic commencé à'.date('H\hi \l\e d M,y',$data['topic_time']).'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a></strong></td>
            <td class="nombremessages">'.$data['topic_post'].'</td>
            <td class="nombrevu">'.$data['topic_vu'].'</td>
            <td><a href="../profil/voirprofil.php?m='.$data['topic_createur'].'&amp;action=consulter">'
            .stripslashes(htmlspecialchars($data['membre_pseudo_createur'])).'</a></td>';

            //Selection dernier message
            $nombreDeMessagesParPage = 15;
            $nbr_post = $data['topic_post'] +1;
            $page = ceil($nbr_post / $nombreDeMessagesParPage);

            echo '<td class="derniermessage">
            Par <a href="../profil/voirprofil.php?m='.$data['post_createur'].'&amp;action=consulter"> '.stripslashes(htmlspecialchars($data['membre_pseudo_last_posteur'])).'</a><br />
            A '.date('H\hi \l\e d M y',$data['post_time']).'</td>
            </tr>';
          }
          ?>

        </table>

        <?php

        //On affiche les pages
        echo '<p>Page : ';
        for ($i = 1 ; $i <= $nombreDePages ; $i++)
        {
          if ($i == $page) //On ne met pas de lien sur la page actuelle
          {
            echo $i;
          }
          else
          {
            echo '
            <a href="../forum/voirforum.php?f='.$forum.'&amp;page='.$i.'">'.$i.'</a>';
          }
        }
        echo '</p>';

      }
      else //S'il n'y a pas de topics
      {
        echo'<p>Ce forum ne contient aucun sujet actuellement.</p>';
      }
      $query->CloseCursor();

      ?>

    </div>
  </div>
</main>
</body>
</html>
