<?php

session_start();

$titre="Poster";

$balises = true;

include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//Qu'est ce qu'on veut faire ? poster, répondre ou éditer ?
$action = (isset($_GET['action']))?htmlspecialchars($_GET['action']):'';

//Il faut être connecté pour poster !
if ($id==0) erreur(ERR_IS_NOT_CO);

//Si on veut poster un nouveau topic, la variable f se trouve dans l'url,
//On récupère certaines valeurs
if (isset($_GET['f']))
{
  $forum = (int) $_GET['f'];

  $query= $db->prepare('SELECT forum_name, auth_view, auth_post, auth_topic, auth_annonce, auth_modo
    FROM forum_forum
    WHERE forum_id =:forum');
    $query->bindValue(':forum',$forum,PDO::PARAM_INT);
    $query->execute();
    $data=$query->fetch();

    //fil d'ariane
    echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/>
    <a href="./voirforum.php?f='.$forum.'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a>
    <img src="../images/flecherouge.png" alt="fleche"/> Nouveau topic</p>';
  }

  //Sinon on veut poster un nouveau message, la variable t est dans l'URL
  elseif (isset($_GET['t']))
  {
    $topic = (int) $_GET['t'];

    $query=$db->prepare('SELECT topic_titre, forum_topic.forum_id,
      forum_name, auth_view, auth_post, auth_topic, auth_annonce, auth_modo
      FROM forum_topic
      LEFT JOIN forum_forum ON forum_forum.forum_id = forum_topic.forum_id
      WHERE topic_id =:topic');
      $query->bindValue(':topic',$topic,PDO::PARAM_INT);
      $query->execute();
      $data=$query->fetch();

      //on recupère f grâce à la requète
      $forum = $data['forum_id'];

      //fil d'ariane
      echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/>
      <a href="./voirforum.php?f='.$forum.'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a>
      <img src="../images/flecherouge.png" alt="fleche"/>
      <a href="./voirtopic.php?t='.$topic.'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a>
      <img src="../images/flecherouge.png" alt="fleche"/> Répondre</p>';
    }

    //Enfin sinon c'est au sujet de la modération
    //On ne connait que le post, il faut chercher le reste
    elseif (isset ($_GET['p']))
    {
      $post = (int) $_GET['p'];

      $query=$db->prepare('SELECT post_createur, forum_post.topic_id, topic_titre, forum_topic.forum_id,
        forum_name, auth_view, auth_post, auth_topic, auth_annonce, auth_modo
        FROM forum_post
        LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id
        LEFT JOIN forum_forum ON forum_forum.forum_id = forum_topic.forum_id
        WHERE forum_post.post_id =:post');
        $query->bindValue(':post',$post,PDO::PARAM_INT);
        $query->execute();
        $data=$query->fetch();

        $topic = $data['topic_id'];
        $forum = $data['forum_id'];

        //fil d'ariane
        echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/>
        <a href="./voirforum.php?f='.$forum.'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a>
        <img src="../images/flecherouge.png" alt="fleche"/>
        <a href="./voirtopic.php?t='.$topic.'">'.stripslashes(htmlspecialchars($data['topic_titre'])).'</a>
        <img src="../images/flecherouge.png" alt="fleche"/> Modérer un message</p>';
      }
      $query->CloseCursor();

      switch($action)
      {
        case "repondre": //Premier cas : on souhaite répondre

        ?>

        <h1>Poster une réponse</h1>

        <div class="form-topic">
          <form method="post" action="poster-traitement.php?action=repondre&amp;t=<?php echo $topic ?>" name="formulaire">

            <fieldset>
              <legend>Message</legend>
              <textarea cols="80" rows="8" id="message" name="message"></textarea>
              <p>
                <input class="icone bouton" type="button" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('[g]', '[/g]');return(false)" />
                <input class="icone bouton" type="button" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('[i]', '[/i]');return(false)" />
                <input class="icone bouton" type="button" id="souligné" name="souligné" value="Souligné" onClick="javascript:bbcode('[s]', '[/s]');return(false)" />
                <input class="icone bouton" type="button" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('[url]', '[/url]');return(false)" />
                <img class="icone smiley" src="../images/smileys/heureux.png" title="heureux" alt="heureux" onClick="javascript:smilies(':D');return(false)" />
                <img class="icone smiley" src="../images/smileys/lol.png" title="lol" alt="lol" onClick="javascript:smilies(':lol:');return(false)" />
                <img class="icone smiley" src="../images/smileys/triste.png" title="triste" alt="triste" onClick="javascript:smilies(':triste:');return(false)" />
                <img class="icone smiley" src="../images/smileys/cool.png" title="cool" alt="cool" onClick="javascript:smilies(':frime:');return(false)" />
                <img class="icone smiley" src="../images/smileys/rire.png" title="rire" alt="rire" onClick="javascript:smilies(':rime:');return(false)" />
                <img class="icone smiley" src="../images/smileys/confus" title="confus" alt="confus" onClick="javascript:smilies(':s');return(false)" />
                <img class="icone smiley" src="../images/smileys/choc.png" title="choc" alt="choc" onClick="javascript:smilies(':O');return(false)" />
                <img class="icone smiley" src="../images/smileys/interrogation.png" title="?" alt="?" onClick="javascript:smilies(':interrogation:');return(false)" />
                <img class="icone smiley" src="../images/smileys/exclamation.png" title="!" alt="!" onClick="javascript:smilies(':exclamation:');return(false)" />
              </p>
            </fieldset>

            <input class="btn" type="submit" name="submit" value="Envoyer" />
            <input type="reset" name = "Effacer" value = "Effacer"/>

          </form>

        </div>

        <?php

        break;

        case "nouveautopic": //Deuxième cas : on souhaite créer un nouveau topic

        ?>

        <h1>Nouveau topic</h1>

        <form method="post" action="poster-traitement.php?action=nouveautopic&amp;f=<?php echo $forum ?>" name="formulaire">

          <fieldset>
            <legend>Titre</legend>
            <input type="text" size="80" id="titre" name="titre" />
          </fieldset>

          <fieldset>
            <legend>Message</legend>
            <textarea cols="80" rows="8" id="message" name="message"></textarea>
            <label><input type="radio" name="mess" value="Annonce" />Annonce</label>
            <label><input type="radio" name="mess" value="Topic" checked="checked" />Topic</label>
            <div class="mise-en-forme">
              <input class="icone bouton" type="button" id="gras" name="gras" value="Gras" onClick="javascript:bbcode('[g]', '[/g]');return(false)" />
              <input class="icone bouton" type="button" id="italic" name="italic" value="Italic" onClick="javascript:bbcode('[i]', '[/i]');return(false)" />
              <input class="icone bouton" type="button" id="souligné" name="souligné" value="Souligné" onClick="javascript:bbcode('[s]', '[/s]');return(false)" />
              <input class="icone bouton" type="button" id="lien" name="lien" value="Lien" onClick="javascript:bbcode('[url]', '[/url]');return(false)" />
              <img class="icone smiley" src="../images/smileys/heureux.png" title="heureux" alt="heureux" onClick="javascript:smilies(':D');return(false)" />
              <img class="icone smiley" src="../images/smileys/lol.png" title="lol" alt="lol" onClick="javascript:smilies(':lol:');return(false)" />
              <img class="icone smiley" src="../images/smileys/triste.png" title="triste" alt="triste" onClick="javascript:smilies(':triste:');return(false)" />
              <img class="icone smiley" src="../images/smileys/cool.png" title="cool" alt="cool" onClick="javascript:smilies(':frime:');return(false)" />
              <img class="icone smiley" src="../images/smileys/rire.png" title="rire" alt="rire" onClick="javascript:smilies(':rire:');return(false)" />
              <img class="icone smiley" src="../images/smileys/confus" title="confus" alt="confus" onClick="javascript:smilies(':s');return(false)" />
              <img class="icone smiley" src="../images/smileys/choc.png" title="choc" alt="choc" onClick="javascript:smilies(':O');return(false)" />
              <img class="icone smiley" src="../images/smileys/interrogation.png" title="?" alt="?" onClick="javascript:smilies(':interrogation:');return(false)" />
              <img class="icone smiley" src="../images/smileys/exclamation.png" title="!" alt="!" onClick="javascript:smilies(':exclamation:');return(false)" />
            </div>
          </fieldset>

          <input class="btn" type="submit" name="submit" value="Envoyer" />
          <input type="reset" name = "Effacer" value = "Effacer" />

        </form>

        <?php

        break;

        default: //Si jamais c'est aucun des cas précédents, c'est qu'il y a eu un problème

        echo'<h2>Cette action est impossible</h2>';

      } //Fin du switch

      ?>

    </div>
  </div>
</main>
</body>
</html>
