<?php

session_start();

$titre="Poster";

include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//On récupère la valeur de la variable action
$action = (isset($_GET['action']))?htmlspecialchars($_GET['action']):'';

// Si le membre n'est pas connecté, il est arrivé ici par erreur
if ($id==0) erreur(ERR_IS_NOT_CO);

switch($action)
{
  //Premier cas : nouveau topic
  case "nouveautopic":

  //On récupère le message, le type de sujet (annonce ou topic) et le titre
  $message = $_POST['message'];
  $mess = $_POST['mess'];
  $titre = $_POST['titre'];

  //on récupère la valeur de la variable f
  $forum = (int) $_GET['f'];
  $temps = time();

  //on récupère le nom du forum pour le fil d'ariane
  $query= $db->prepare('SELECT forum_name
    FROM forum_forum
    WHERE forum_id =:forum');
    $query->bindValue(':forum',$forum,PDO::PARAM_INT);
    $query->execute();
    $data=$query->fetch();

    //fil d'ariane
    echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/>
    <a href="./voirforum.php?f='.$forum.'">'.stripslashes(htmlspecialchars($data['forum_name'])).'</a>
    <img src="../images/flecherouge.png" alt="fleche"/> Nouveau topic</p>';

    $query->CloseCursor();

    echo '<h1>Nouveau topic</h1>';

    if (empty($message) || empty($titre))
    {
      echo'<div class="error"><p>Votre message ou votre titre est vide.</p>
      <p>Cliquez <a href="./poster.php?action=nouveautopic&amp;f='.$forum.'">ici</a> pour recommencer.</p></div>';
    }
    else //Si le message et le titre ne sont pas vides
    {
      //On insère le topic dans forum_topic en laissant le champ topic_last_post à 0
      $query=$db->prepare('INSERT INTO forum_topic (forum_id, topic_titre, topic_createur, topic_vu, topic_time, topic_genre, topic_last_post, topic_first_post, topic_post)
      VALUES (:forum, :titre, :id, 1, :temps, :mess, 0, 0, 0)');
      $query->bindValue(':forum', $forum, PDO::PARAM_INT);
      $query->bindValue(':titre', $titre, PDO::PARAM_STR);
      $query->bindValue(':id', $id, PDO::PARAM_INT);
      $query->bindValue(':temps', $temps, PDO::PARAM_INT);
      $query->bindValue(':mess', $mess, PDO::PARAM_STR);
      $query->execute();

      $nouveautopic = $db->lastInsertId(); //on récupère forum_id
      $query->CloseCursor();

      //Puis on insère le message dans forum_post
      $query=$db->prepare('INSERT INTO forum_post (post_createur, post_texte, post_time, topic_id, post_forum_id)
      VALUES (:id, :message, :temps, :nouveautopic, :forum)');
      $query->bindValue(':id', $id, PDO::PARAM_INT);
      $query->bindValue(':message', $message, PDO::PARAM_STR);
      $query->bindValue(':temps', $temps,PDO::PARAM_INT);
      $query->bindValue(':nouveautopic', (int) $nouveautopic, PDO::PARAM_INT);
      $query->bindValue(':forum', $forum, PDO::PARAM_INT);
      $query->execute();

      $nouveaupost = $db->lastInsertId(); //on récupère post_createur
      $query->CloseCursor();

      //on update la valeur de topic_last_post et de topic_first_post dans forum_topic
      $query=$db->prepare('UPDATE forum_topic
        SET topic_last_post = :nouveaupost, topic_first_post = :nouveaupost
        WHERE topic_id = :nouveautopic');
        $query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);
        $query->bindValue(':nouveautopic', (int) $nouveautopic, PDO::PARAM_INT);
        $query->execute();
        $query->CloseCursor();

        //Enfin on met à jour les tables forum_forum et forum_membres
        $query=$db->prepare('UPDATE forum_forum
          SET forum_post = forum_post + 1 ,forum_topic = forum_topic + 1, forum_last_post_id = :nouveaupost
          WHERE forum_id = :forum');
          $query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);
          $query->bindValue(':forum', (int) $forum, PDO::PARAM_INT);
          $query->execute();
          $query->CloseCursor();

          $query=$db->prepare('UPDATE forum_membres
            SET membre_post = membre_post + 1
            WHERE membre_id = :id');
            $query->bindValue(':id', $id, PDO::PARAM_INT);
            $query->execute();
            $query->CloseCursor();

            //on indique à l'utilisateur que le message a été posté
            echo'<p>Votre message a bien été ajouté !</p>
            <p>Cliquez <a href="./voirtopic.php?t='.$nouveautopic.'">ici</a> pour le voir.</p>
            <p>Cliquez <a href="./index.php">ici</a> pour revenir à l index du forum.</p>';
          }
          break;

          //Deuxième cas : répondre
          case "repondre":
          $message = $_POST['message'];

          //on récupère la valeur de la variable t
          $topic = (int) $_GET['t'];
          $temps = time();

          if (empty($message))
          {
            echo'<p>Votre message est vide, cliquez <a href="./poster.php?action=repondre&amp;t='.$topic.'">ici</a> pour recommencer</p>';
          }
          else //Sinon, si le message n'est pas vide
          {
            //On récupère l'id du forum
            $query=$db->prepare('SELECT forum_id, topic_post
              FROM forum_topic
              WHERE topic_id = :topic');
              $query->bindValue(':topic', $topic, PDO::PARAM_INT);
              $query->execute();
              $data=$query->fetch();
              $forum = $data['forum_id'];

              //Puis on insère le message dans la table forum_post
              $query=$db->prepare('INSERT INTO forum_post (post_createur, post_texte, post_time, topic_id, post_forum_id)
              VALUES(:id,:mess,:temps,:topic,:forum)');
              $query->bindValue(':id', $id, PDO::PARAM_INT);
              $query->bindValue(':mess', $message, PDO::PARAM_STR);
              $query->bindValue(':temps', $temps, PDO::PARAM_INT);
              $query->bindValue(':topic', $topic, PDO::PARAM_INT);
              $query->bindValue(':forum', $forum, PDO::PARAM_INT);
              $query->execute();

              $nouveaupost = $db->lastInsertId();
              $query->CloseCursor();

              //On change un peu la table forum_topic
              $query=$db->prepare('UPDATE forum_topic
                SET topic_post = topic_post + 1, topic_last_post = :nouveaupost
                WHERE topic_id =:topic');
                $query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);
                $query->bindValue(':topic', (int) $topic, PDO::PARAM_INT);
                $query->execute();
                $query->CloseCursor();

                //Puis même combat sur les 2 autres tables
                $query=$db->prepare('UPDATE forum_forum
                  SET forum_post = forum_post + 1 , forum_last_post_id = :nouveaupost
                  WHERE forum_id = :forum');
                  $query->bindValue(':nouveaupost', (int) $nouveaupost, PDO::PARAM_INT);
                  $query->bindValue(':forum', (int) $forum, PDO::PARAM_INT);
                  $query->execute();
                  $query->CloseCursor();

                  $query=$db->prepare('UPDATE forum_membres
                    SET membre_post = membre_post + 1
                    WHERE membre_id = :id');
                    $query->bindValue(':id', $id, PDO::PARAM_INT);
                    $query->execute();
                    $query->CloseCursor();

                    $nombreDeMessagesParPage = 15;
                    $nbr_post = $data['topic_post']+1;
                    $page = ceil($nbr_post / $nombreDeMessagesParPage);

                    //on indique à l'utilisateur que le message a été posté
                    echo'<p>Votre message a bien été ajouté!</p>
                    <p>Cliquez <a href="./voirtopic.php?t='.$topic.'&amp;page='.$page.'#p_'.$nouveaupost.'">ici</a> pour le voir</p>
                    <p>Cliquez <a href="../accueil/index.php">ici</a> pour revenir à l\'index du forum</p>';
                  }
                  break;

                  default;
                  echo'<p>Cette action est impossible</p>';
                }

                ?>


              </div>
            </div>
          </main>
        </body>
        </html>
