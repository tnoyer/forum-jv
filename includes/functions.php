<?php
function erreur($err='')
{
   $mess=($err!='')?$err:'Une erreur inconnue s\'est produite';
   exit('<div class="error"><p>'.$mess.'</p>
   <p>Cliquez <a href="../accueil/index.php">ici</a> pour revenir à la page d\'accueil.</p></div></div></body></html>');
}
?>

<?php
function move_avatar($avatar)
{
    $extension_upload = strtolower(substr(strrchr($avatar['name'], '.')  ,1));
    $name = time();
    $nomavatar = str_replace(' ','',$name).".".$extension_upload;
    $name = "../images/avatars/".str_replace(' ','',$name).".".$extension_upload;
    move_uploaded_file($avatar['tmp_name'],$name);
    return $nomavatar;
}
?>

<?php
function code($texte) //permet de reprendre le texte écrit en bbcode et de le traduire pour l'afficher
{
//Smileys
$texte = str_replace(':D ', '<img src="../images/smileys/heureux.png" title="heureux" alt="heureux" />', $texte);
$texte = str_replace(':lol: ', '<img src="../images/smileys/lol.png" title="lol" alt="lol" />', $texte);
$texte = str_replace(':triste:', '<img src="../images/smileys/triste.png" title="triste" alt="triste" />', $texte);
$texte = str_replace(':frime:', '<img src="../images/smileys/cool.png" title="cool" alt="cool" />', $texte);
$texte = str_replace(':rire:', '<img src="../images/smileys/rire.png" title="rire" alt="rire" />', $texte);
$texte = str_replace(':s', '<img src="../images/smileys/confus.png" title="confus" alt="confus" />', $texte);
$texte = str_replace(':O', '<img src="../images/smileys/choc.png" title="choc" alt="choc" />', $texte);
$texte = str_replace(':interrogation:', '<img src="../images/smileys/interrogation.png" title="?" alt="?" />', $texte);
$texte = str_replace(':exclamation:', '<img src="../images/smileys/exclamation.png" title="!" alt="!" />', $texte);

//Mise en forme du texte
//gras
$texte = preg_replace('`\[g\](.+)\[/g\]`isU', '<strong>$1</strong>', $texte);
//italique
$texte = preg_replace('`\[i\](.+)\[/i\]`isU', '<em>$1</em>', $texte);
//souligné
$texte = preg_replace('`\[s\](.+)\[/s\]`isU', '<u>$1</u>', $texte);
//lien
$texte = preg_replace('#http://[a-z0-9._/-]+#i', '<a href="$0">$0</a>', $texte);
//citation
$texte = preg_replace('`\[quote\](.+)\[/quote\]`isU', '<div id="quote">$1</div>', $texte);

//On retourne la variable texte
return $texte;
}
?>
