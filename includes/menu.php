<body>

  <header class="block-header"
  <div id="banniere">
  </div>
</header>

<nav class="block-nav">
  <div class="inner">
    <ul class="ul-princ">
      <li class="li-princ"><a href="../accueil/index.php" class="a-princ">Accueil</a></li>
      <li class="li-princ"><a href="#" class="a-princ">Mes options</a>
        <ul class="ul-second">
          <li><a href="#">Page 1</a></li> <!--liste déroulante-->
          <li><a href="#">Page 2</a></li>
        </ul>
      </li>
      <li class="li-princ"><a href="#" class="a-princ">Navigation</a>
        <ul class="ul-second">
          <li><a href="#">Page 1</a></li>
          <li><a href="#">Page 2</a></li>
        </ul>
      </li>
      <?php
      //on vérifie si le membre est connecté pour changer l'affichage de la liste déroulante
      if (isset($_SESSION['id']))
      {
        ?>
        <li class="li-princ"><a href="#" class="a-princ"><?php echo 'Bonjour '.$_SESSION['pseudo'] ?></a>
          <ul class="ul-second">
            <?php
            echo '<li><a href="../profil/voirprofil.php?m='.$_SESSION['id'].'&amp;action=consulter">Voir mon profil</a></li>';

            echo '<li><a href="../profil/deconnexion.php">Se déconnecter</a></li>';
          }
          else
          {
            echo '<li class="li-princ"><a href="#" class="a-princ">Mon compte</a>';
            echo '<ul class="ul-second">';
            echo '<li><a href="../profil/connexion.php">Se connecter</a></li>';
            echo '<li><a href="../profil/register.php">S\'inscrire</a></li>';
          }
          ?>
        </ul>
      </li>
    </ul>
  </div>
</nav> <!--fin block nav-->

<main class="block-main">
  <div class="inner">
    <div class="corps-forum">
