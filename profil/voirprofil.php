<?php

session_start();

$titre="Profil";
include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//On récupère la valeur de nos variables passées par URL
$action = isset($_GET['action'])?htmlspecialchars($_GET['action']):'';
$membre = isset($_GET['m'])?(int) $_GET['m']:'';

//On regarde la valeur de la variable $action
switch($action)
{
  //Si c'est "consulter"
  case "consulter":

  //On récupère les infos du membre
  $query=$db->prepare('SELECT membre_pseudo, membre_avatar, membre_email, membre_signature, membre_siteweb, membre_post, membre_inscrit, membre_localisation
    FROM forum_membres WHERE membre_id=:membre');
    $query->bindValue(':membre',$membre, PDO::PARAM_INT);
    $query->execute();
    $data=$query->fetch();

    //fil d'ariane
    echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> '.stripslashes(htmlspecialchars($data['membre_pseudo']));

    echo'<h1>Profil de '.stripslashes(htmlspecialchars($data['membre_pseudo'])).'</h1>';

    echo '<h2><img src="../images/avatars/'.$data['membre_avatar'].'" alt="Avatar" /></h2>';

    //On affiche les infos sur le membre
    echo '<table class="table-profil"><tbody class="body-profil">

    <tr>
    <td><strong>Adresse E-Mail</strong></td>
    <td><a href="mailto:'.stripslashes($data['membre_email']).'">
    '.stripslashes(htmlspecialchars($data['membre_email'])).'</a>
    </tr>

    <tr>
    <td><strong>Site Web</strong>
    <td><a href="'.stripslashes($data['membre_siteweb']).'">'.stripslashes(htmlspecialchars($data['membre_siteweb'])).'</a>
    </tr>

    <tr>
    <td><strong>Date d\'inscription</strong>
    <td>A '.date('H:i \l\e d M Y',$data['membre_inscrit']).'</td>
    </tr>

    <tr>
    <td><strong>Message(s) posté(s)</strong></td>
    <td>'.$data['membre_post'].'</td>
    </tr>

    <tr>
    <td><strong>Localisation</strong></td>
    <td>'.stripslashes(htmlspecialchars($data['membre_localisation'])).'</td>
    </tr>

    <tr>
    <td><strong>Signature</strong></td>
    <td>'.stripslashes(htmlspecialchars($data['membre_signature'])).'</td>
    </tr>

    </tbody></table>';

    $query->CloseCursor();

    echo '<div class="btn-content"><div class="btn"><a href="../profil/voirprofil.php?action=modifier">Modifier mon profil</a></div></div>';

    break;

    //Si on choisit de modifier son profil
    case "modifier":
      if (empty($_POST['sent'])) // Si la variable est vide, c'est qu'on a pas encore été dans le formulaire
      {
        //On commence par s'assurer que le membre est connecté
        if ($id==0) erreur(ERR_IS_NOT_CO);

        //On prend les infos du membre
        $query=$db->prepare('SELECT membre_pseudo, membre_email, membre_siteweb, membre_signature, membre_localisation, membre_avatar
          FROM forum_membres
          WHERE membre_id=:id');
          $query->bindValue(':id',$id,PDO::PARAM_INT);
          $query->execute();
          $data=$query->fetch();

          //fil d'ariane
          echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> '.stripslashes(htmlspecialchars($data['membre_pseudo']));

          echo '<h1>Modifier mon profil</h1>';

          echo '<form method="post" action="voirprofil.php?action=modifier" enctype="multipart/form-data">

          <fieldset>
          <legend>Identifiants</legend>
          <p>Pseudo : <strong>'.stripslashes(htmlspecialchars($data['membre_pseudo'])).'</strong></p>
          <p><label for="password">Nouveau mot de Passe </label><br/><input type="password" name="password" id="password"/></p>
          <p><label for="confirm">Confirmer le mot de passe </label><br/><input type="password" name="confirm" id="confirm"/></p>
          </fieldset>

          <fieldset>
          <legend>Contacts</legend>
          <p><label for="email">Votre adresse E_Mail </label><br/><input type="text" name="email" id="email"
          value="'.stripslashes($data['membre_email']).'"/></p>

          <p><label for="website">Votre site web </label><br/><input type="text" name="website" id="website"
          value="'.stripslashes($data['membre_siteweb']).'" /></p>
          </fieldset>

          <fieldset>
          <legend>Informations supplémentaires</legend>
          <p><label for="localisation">Localisation </label><br/><input type="text" name="localisation" id="localisation"
          value="'.stripslashes($data['membre_localisation']).'" /></p>
          </fieldset>

          <fieldset>
          <legend>Profil sur le forum</legend>
          <p><label for="avatar">Changer votre avatar </label><input type="file" name="avatar" id="avatar" />
          (Taille max : 10 ko)</p>
          <p><label><input type="checkbox" name="delete" value="Delete" />
          Supprimer l avatar</label>
          <img src="../images/avatars/'.$data['membre_avatar'].'" alt="Avatar" /></p>

          <p><label for="signature">Signature </label><br/><textarea cols="40" rows="4" name="signature" id="signature">
          '.stripslashes($data['membre_signature']).'</textarea></p>
          </fieldset>

          <p><input class="btn" type="submit" value="Modifier" />
          <input type="hidden" id="sent" name="sent" value="1" /></p>
          </form>';

          $query->CloseCursor();
        }
        else //Sinon on est dans la page de traitement car la variable sent a été rempli à la fin du formulaire de modification
        {
          $pseudo_erreur1 = NULL;
          $pseudo_erreur2 = NULL;
          $mdp_erreur = NULL;
          $email_erreur1 = NULL;
          $email_erreur2 = NULL;
          $signature_erreur = NULL;
          $avatar_erreur = NULL;
          $avatar_erreur1 = NULL;
          $avatar_erreur2 = NULL;
          $avatar_erreur3 = NULL;

          //On récupère les variables
          $i = 0; //variable incrémenté à chaque erreur détecté
          $temps = time();
          $signature = $_POST['signature'];
          $email = $_POST['email'];
          $website = $_POST['website'];
          $localisation = $_POST['localisation'];
          $pass = md5($_POST['password']);
          $confirm = md5($_POST['confirm']);

          //Vérification du mdp
          if ($pass != $confirm || empty($_POST['confirm']) || empty($_POST['password']))
          {
            $mdp_erreur = "Votre mot de passe et votre confirmation sont différent, ou sont vides.";
            $i++;
          }

          //On vérifie si l'adresse email n'appartient pas à un autre membre sauf si elle n'a pas été modifiée
          //On commence donc par récupérer le mail
          $query=$db->prepare('SELECT membre_email
            FROM forum_membres
            WHERE membre_id =:id');
          $query->bindValue(':id',$id,PDO::PARAM_INT);
          $query->execute();
          $data=$query->fetch();
          if (strtolower($data['membre_email']) != strtolower($email))
          {
            //Il faut que l'adresse email n'ait jamais été utilisée
            $query=$db->prepare('SELECT COUNT(*) AS nbr FROM forum_membres WHERE membre_email =:mail');
            $query->bindValue(':mail',$email,PDO::PARAM_STR);
            $query->execute();
            $mail_free=($query->fetchColumn()==0)?1:0;
            $query->CloseCursor();
            if(!$mail_free)
            {
              $email_erreur1 = "Votre adresse email est déjà utilisé par un membre.";
              $i++;
            }

            //On vérifie la forme maintenant
            if (!preg_match("#^[a-z0-9A-Z._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $email) || empty($email))
            {
              $email_erreur2 = "Votre nouvelle adresse E-Mail n'a pas un format valide.";
              $i++;
            }
          }

          //Vérification de la signature
          if (strlen($signature) > 200)
          {
            $signature_erreur = "Votre nouvelle signature est trop longue.";
            $i++;
          }


          //Vérification de l'avatar
          if (!empty($_FILES['avatar']['size']))
          {
            //On définit les variables :
            $maxsize = 30072; //Poid de l'image
            $maxwidth = 100; //Largeur de l'image
            $maxheight = 100; //Longueur de l'image
            //Liste des extensions valides
            $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'bmp' );

            if ($_FILES['avatar']['error'] > 0)
            {
              $avatar_erreur = "Erreur lors du tranfsert de l'avatar : ";
            }
            if ($_FILES['avatar']['size'] > $maxsize)
            {
              $i++;
              $avatar_erreur1 = "Le fichier est trop gros :
              (<strong>".$_FILES['avatar']['size']." Octets</strong>
              contre <strong>".$maxsize." Octets</strong>)";
            }

            $image_sizes = getimagesize($_FILES['avatar']['tmp_name']);
            if ($image_sizes[0] > $maxwidth OR $image_sizes[1] > $maxheight)
            {
              $i++;
              $avatar_erreur2 = "Image trop large ou trop longue :
              (<strong>".$image_sizes[0]."x".$image_sizes[1]."</strong> contre
              <strong>".$maxwidth."x".$maxheight."</strong>)";
            }

            $extension_upload = strtolower(substr(  strrchr($_FILES['avatar']['name'], '.')  ,1));
            if (!in_array($extension_upload,$extensions_valides) )
            {
              $i++;
              $avatar_erreur3 = "Extension de l'avatar incorrecte.";
            }
          }

          if ($i == 0) // Si $i est vide, il n'y a pas d'erreur
          {
            if (!empty($_FILES['avatar']['size']))
            {
              $nomavatar=move_avatar($_FILES['avatar']);
              $query=$db->prepare('UPDATE forum_membres
                SET membre_avatar = :avatar
                WHERE membre_id = :id');
                $query->bindValue(':avatar',$nomavatar,PDO::PARAM_STR);
                $query->bindValue(':id',$id,PDO::PARAM_INT);
                $query->execute();
                $query->CloseCursor();
              }

              //Une nouveauté ici : on peut choisis de supprimer l'avatar
              if (isset($_POST['delete']))
              {
                $query=$db->prepare('UPDATE forum_membres
                  SET membre_avatar=0 WHERE membre_id = :id');
                  $query->bindValue(':id',$id,PDO::PARAM_INT);
                  $query->execute();
                  $query->CloseCursor();
                }

                //On modifie la table

                $query=$db->prepare('UPDATE forum_membres
                  SET  membre_mdp = :mdp, membre_email=:mail, membre_siteweb=:website,
                  membre_signature=:sign, membre_localisation=:loc
                  WHERE membre_id=:id');
                  $query->bindValue(':mdp',$pass,PDO::PARAM_STR);
                  $query->bindValue(':mail',$email,PDO::PARAM_STR);
                  $query->bindValue(':website',$website,PDO::PARAM_STR);
                  $query->bindValue(':sign',$signature,PDO::PARAM_STR);
                  $query->bindValue(':loc',$localisation,PDO::PARAM_STR);
                  $query->bindValue(':id',$id,PDO::PARAM_INT);
                  $query->execute();
                  $query->CloseCursor();

                  echo'<h1>Modification terminée</h1>';
                  echo'<p>Votre profil a été modifié avec succès !</p>';
                  echo'<p>Cliquez <a href="../accueil/index.php">ici</a> pour revenir à la page d accueil.</p>';
                }
                else
                {
                  echo'<h1>Modification interrompue</h1>';
                  echo'<div class="error"><p>Une ou plusieurs erreurs se sont produites pendant la modification du profil.</p>';
                  echo'<p>'.$i.' erreur(s):</p>';
                  echo'<p>'.$mdp_erreur.'</p>';
                  echo'<p>'.$email_erreur1.'</p>';
                  echo'<p>'.$email_erreur2.'</p>';
                  echo'<p>'.$signature_erreur.'</p>';
                  echo'<p>'.$avatar_erreur.'</p>';
                  echo'<p>'.$avatar_erreur1.'</p>';
                  echo'<p>'.$avatar_erreur2.'</p>';
                  echo'<p>'.$avatar_erreur3.'</p>';
                  echo'<p> Cliquez <a href="./voirprofil.php?action=modifier">ici</a> pour recommencer.</p></div>';
                }
              }
              break;

              default: //Si jamais c'est aucun de ceux-là c'est qu'il y a eu un problème :o
              echo'<p>Cette action est impossible.</p>';

            } //Fin du switch

            ?>

          </div>
        </div>
      </main>
    </body>
    </html>
