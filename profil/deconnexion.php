<?php

session_start();

session_destroy();

if (isset ($_COOKIE['pseudo']))
{
setcookie('pseudo', '', -1);
}

$titre="Déconnexion";
include("../includes/debut.php");
include("../includes/menu.php");

//fil d'ariane
echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> <a href="./deconnexion.php">'.$titre.'</a>';

echo '<h1 class="title_deconnexion">Déconnexion</h1>';
if ($id==0)
{
  erreur(ERR_IS_NOT_CO);
}
else
{
  echo '<div class="block-deconnexion"><p>Vous êtes à présent déconnecté.</p>
  <p>Cliquez <a href="./connexion.php">ici</a> pour se connecter.</p></div>';
}
?>

</div>
</div>
</main>
</body>
</html>
