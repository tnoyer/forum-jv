<?php
session_start();
$titre="Connexion";
include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//fil d'ariane
echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> <a href="connexion.php">'.$titre.'</a>';

echo '<h1 class="title_connexion">Connexion</h1>';

if ($id!=0) erreur(ERR_IS_CO);

//page de formulaire si utilisateur non connecté
//permet à l'utilisateur de se connecter
if (!isset($_POST['pseudo']))
{
	echo '<div class="form-connexion">
	<form method="post" action="connexion-traitement.php">
	<fieldset>
	<legend>Connexion</legend>
	<p><label for="pseudo">Pseudo </label><br/><input name="pseudo" type="text" id="pseudo" /></p>
	<p><label for="password">Mot de Passe </label><br/><input type="password" name="password" id="password" /></p>
	<p><input class="btn" type="submit" value="Connexion" /></p>
	<p><a href="./register.php">Pas encore inscrit ?</a></p>
	</fieldset>
	</form>
	</div>';
}
?>

</div>
</div>
</main>
</body>
</html>
