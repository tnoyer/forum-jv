<?php
session_start();
$titre="Connexion";
include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//fil d'ariane
echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> <a href="./connexion.php">'.$titre.'</a>';

echo '<h1 class="title_connexion">Connexion</h1>';

$message='';

if (empty($_POST['pseudo']) || empty($_POST['password'])) //Oublie d'un champ
{
	$message = '<div class="error_co"><p>Une erreur s\'est produite pendant votre identification.
	Vous devez remplir tous les champs.</p>
	<p>Cliquez <a href="./connexion.php">ici</a> pour revenir.</p></div>';
}
else //On check le mot de passe
{
	$query=$db->prepare('SELECT membre_mdp, membre_id, membre_rang, membre_pseudo
		FROM forum_membres
		WHERE membre_pseudo = :pseudo');
		$query->bindValue(':pseudo',$_POST['pseudo'], PDO::PARAM_STR);
		$query->execute();
		$data=$query->fetch();
		if ($data['membre_mdp'] == md5($_POST['password'])) // recherche si mdp correspond au pseudo du membre
		{
			//voir attribution des variables de sessions dans debut.php 
			$_SESSION['pseudo'] = $data['membre_pseudo'];
			$_SESSION['level'] = $data['membre_rang'];
			$_SESSION['id'] = $data['membre_id'];
			$message = '<p>Bienvenue '.$data['membre_pseudo'].',
			vous êtes maintenant connecté!</p>
			<p>Cliquez <a href="../accueil/index.php">ici</a>
			pour revenir à la page d accueil.</p>';

			//cookie
			if (isset($_POST['souvenir']))
			{
				$expire = time() + 365*24*3600;
				setcookie('pseudo', $_SESSION['pseudo'], $expire);
			}
		}
		else // Acces pas OK !
		{
			$message = '<p>Une erreur s\'est produite pendant votre identification.<br />
			Le mot de passe ou le pseudo entré n\'est pas correcte.</p>
			<p>Cliquez <a href="./connexion.php">ici</a> pour revenir à la page précédente.</p>';
		}
		$query->CloseCursor();
	}
	echo '<div class="block-connexion">'.$message.'</div>';
	?>

</div>
</div>
</main>
</body>
</html>
