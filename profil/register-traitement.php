<?php

session_start();

$titre="Inscription";
include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//fil d'ariane
echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> <a href="./register.php">'.$titre.'</a>';

$pseudo_erreur1 = NULL;
$pseudo_erreur2 = NULL;
$mdp_erreur = NULL;
$email_erreur1 = NULL;
$email_erreur2 = NULL;
$signature_erreur = NULL;
$avatar_erreur = NULL;
$avatar_erreur1 = NULL;
$avatar_erreur2 = NULL;
$avatar_erreur3 = NULL;

//On récupère les variables
$i = 0; //variable incrémenté à chaque erreur détecté
$temps = time();
$pseudo= $_POST['pseudo'];
$signature = $_POST['signature'];
$email = $_POST['email'];
$website = $_POST['website'];
$localisation = $_POST['localisation'];
$pass = md5($_POST['password']);
$confirm = md5($_POST['confirm']);
$nb_messages= $_POST['nb_messages'];

//Vérification du pseudo
$query=$db->prepare('SELECT COUNT(*) AS nbr FROM forum_membres WHERE membre_pseudo =:pseudo');
$query->bindValue(':pseudo',$pseudo, PDO::PARAM_STR);
$query->execute();
$pseudo_free=($query->fetchColumn()==0)?1:0;
$query->CloseCursor();
if(!$pseudo_free) // if($pseudo_free == 0)
{
  $pseudo_erreur1 = "Votre pseudo est déjà utilisé par un membre.";
  $i++;
}

if (strlen($pseudo) < 3 || strlen($pseudo) > 15)
{
  $pseudo_erreur2 = "Votre pseudo est soit trop grand, soit trop petit.";
  $i++;
}

//Vérification du mdp
if ($pass != $confirm || empty($confirm) || empty($pass))
{
  $mdp_erreur = "Votre mot de passe et votre confirmation sont diffèrent, ou sont vides.";
  $i++;
}

//Vérification de l'adresse email
$query=$db->prepare('SELECT COUNT(*) AS nbr FROM forum_membres WHERE membre_email =:mail');
$query->bindValue(':mail',$email, PDO::PARAM_STR);
$query->execute();
$mail_free=($query->fetchColumn()==0)?1:0;
$query->CloseCursor();

if(!$mail_free)
{
  $email_erreur1 = "Votre adresse email est déjà utilisée par un membre.";
  $i++;
}
if (!preg_match("#^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-z]{2,4}$#", $email) || empty($email))
{
  $email_erreur2 = "Votre adresse E-Mail n'a pas un format valide.";
  $i++;
}

//Vérification de la signature
if (strlen($signature) > 200)
{
  $signature_erreur = "Votre signature est trop longue.";
  $i++;
}

//Vérification de l'avatar :
if (!empty($_FILES['avatar']['size']))
{
  //On définit les variables :
  $maxsize = 10024; //Poid de l'image
  $maxwidth = 100; //Largeur de l'image
  $maxheight = 100; //Longueur de l'image
  $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png', 'bmp' ); //Liste des extensions valides

  if ($_FILES['avatar']['error'] > 0)
  {
    $avatar_erreur = "Erreur lors du transfert de l'avatar : ";
  }
  if ($_FILES['avatar']['size'] > $maxsize)
  {
    $i++;
    $avatar_erreur1 = "Le fichier est trop gros : (<strong>".$_FILES['avatar']['size']." Octets</strong>    contre <strong>".$maxsize." Octets.</strong>)";
  }

  $image_sizes = getimagesize($_FILES['avatar']['tmp_name']);
  if ($image_sizes[0] > $maxwidth OR $image_sizes[1] > $maxheight)
  {
    $i++;
    $avatar_erreur2 = "Image trop large ou trop longue :
    (<strong>".$image_sizes[0]."x".$image_sizes[1]."</strong> contre <strong>".$maxwidth."x".$maxheight."</strong>)";
  }

  $extension_upload = strtolower(substr(strrchr($_FILES['avatar']['name'],'.'),1));
  if (!in_array($extension_upload,$extensions_valides))
  {
    $i++;
    $avatar_erreur3 = "Extension de l'avatar incorrecte.";
  }
}

if ($i==0) // si aucune erreur détectée lors des vérifs
{
  echo'<h1>Inscription terminée !</h1>';
  echo'<p>Bienvenue '.stripslashes(htmlspecialchars($_POST['pseudo'])).' ! Vous êtes maintenant inscrit sur le forum.</p>
  <p>Cliquez <a href="../accueil/index.php">ici</a> pour revenir à la page d accueil.</p>';

  $nomavatar=(!empty($_FILES['avatar']['size']))?move_avatar($_FILES['avatar']):'';

  //On insert les données dans la BDD
  $query=$db->prepare('INSERT INTO forum_membres (membre_pseudo, membre_mdp, membre_email, membre_siteweb, membre_avatar, membre_signature, membre_localisation, membre_inscrit, membre_derniere_visite, membre_post)
  VALUES (:pseudo, :pass, :email, :website, :nomavatar, :signature, :localisation, :temps, :temps, :nb_messages)');
  $query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
  $query->bindValue(':pass', $pass, PDO::PARAM_STR);
  $query->bindValue(':email', $email, PDO::PARAM_STR);
  $query->bindValue(':website', $website, PDO::PARAM_STR);
  $query->bindValue(':nomavatar', $nomavatar, PDO::PARAM_STR);
  $query->bindValue(':signature', $signature, PDO::PARAM_STR);
  $query->bindValue(':localisation', $localisation, PDO::PARAM_STR);
  $query->bindValue(':temps', $temps, PDO::PARAM_STR);
  $query->bindValue(':nb_messages', $nb_messages, PDO::PARAM_INT);
  $query->execute()or die(print_r($query->errorInfo()));

  //Et on définit les variables de sessions
  $_SESSION['pseudo'] = $pseudo;
  $_SESSION['id'] = $db->lastInsertId();
  $_SESSION['level'] = 2;

  $query->CloseCursor();

  //Mail de confirmation
  // $message = "Bienvenue sur Forum J-V !";
  // //Titre
  // $titre = "Inscription à Forum J-V !";
  //
  // mail($_POST['email'], $titre, $message);
}
else
{
  echo'<h1>Inscription interrompue</h1>';
  echo'<div class="error"><p>Une ou plusieurs erreurs se sont produites pendant l incription !</p>';
  echo'<p>'.$i.' erreur(s): </p>';
  echo'<p>'.$pseudo_erreur1.'</p>';
  echo'<p>'.$pseudo_erreur2.'</p>';
  echo'<p>'.$mdp_erreur.'</p>';
  echo'<p>'.$email_erreur1.'</p>';
  echo'<p>'.$email_erreur2.'</p>';
  echo'<p>'.$signature_erreur.'</p>';
  echo'<p>'.$avatar_erreur.'</p>';
  echo'<p>'.$avatar_erreur1.'</p>';
  echo'<p>'.$avatar_erreur2.'</p>';
  echo'<p>'.$avatar_erreur3.'</p>';

  echo'<p>Cliquez <a href="register.php">ici</a> pour recommencer.</p></div>';
}
?>

</div>
</div>
</main>
</body>
</html>
