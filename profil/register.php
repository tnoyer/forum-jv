<?php

session_start();

$titre="Inscription";
include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//fil d'ariane
echo '<a href="../accueil/index.php">Accueil du forum</a> <img src="../images/flecherouge.png" alt="fleche"/> <a href="./register.php">'.$titre.'</a>';

if ($id!=0) erreur(ERR_IS_CO);

if (empty($_POST['pseudo'])) // Si la variable est vide, on peut considérer qu'on est sur la page de formulaire
{
  echo '<h1>Inscription 1/2</h1>';
  echo '<form method="post" action="register-traitement.php" enctype="multipart/form-data">
  <fieldset>
  <legend>Identifiants</legend>
  <p><label for="pseudo">Pseudo* </label> <br/> <input name="pseudo" type="text" id="pseudo" /> (le pseudo doit contenir entre 3 et 15 caractères)</p>
  <p><label for="password">Mot de Passe* </label><br/><input type="password" name="password" id="password" /></p>
  <p><label for="confirm">Confirmer le mot de passe* </label><br/><input type="password" name="confirm" id="confirm" /></p>
  </fieldset>
  <fieldset>
  <legend>Contacts</legend>
  <p><label for="email">Votre adresse Mail* </label><br/><input type="text" name="email" id="email" /></p>
  <p><label for="website">Votre site web </label><br/><input type="text" name="website" id="website" />
  </fieldset>
  <fieldset>
  <legend>Informations supplémentaires</legend>
  <p><label for="localisation">Localisation </label><br/><input type="text" name="localisation" id="localisation" />
  </fieldset>
  <fieldset>
  <legend>Profil sur le forum</legend>
  <p><label for="avatar">Choisissez votre avatar </label><br/><input type="file" name="avatar" id="avatar" />(Taille max : 10Ko)</p>
  <p><label for="signature">Signature </label><br/><textarea cols="40" rows="4" name="signature" id="signature" placeholder="La signature est limitée à 200 caractères"></textarea>
  </fieldset>
  <p>Les champs suivis d un * sont obligatoires.</p>
  <p><input type="checkbox" name="souvenir" /><label>Se souvenir de moi ?</label></p>
  <p><input type="hidden" name="nb_messages" value="0" /></p>
  <p><input class="btn" type="submit" value="S\'inscrire" /></p>
  </form>';
} //Fin de la partie formulaire
?>

</div>
</div>
</main>
</body>
</html>
