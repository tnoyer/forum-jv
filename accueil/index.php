<?php

//Cette fonction doit être appelée avant tout code html
session_start();

//On donne ensuite un titre à la page, puis on appelle notre fichier debut.php
$titre = "Accueil du forum";

include("../includes/identifiants.php");
include("../includes/debut.php");
include("../includes/menu.php");

//fil d'ariane
echo'<a href ="index.php">'.$titre.'</a>';
?>

<h1 class="title-forum">Forum J-V</h1>

<?php
//Initialisation de deux variables
$totaldesmessages = 0;
$categorie = NULL;

//Cette requête permet d'obtenir tout sur le forum
$query=$db->prepare('SELECT cat_id, cat_nom,
  forum_forum.forum_id, forum_name, forum_desc, forum_post, forum_topic, auth_view, forum_topic.topic_id,  forum_topic.topic_post, post_id, post_time, post_createur, membre_pseudo, membre_id
  FROM forum_categorie
  LEFT JOIN forum_forum ON forum_categorie.cat_id = forum_forum.forum_cat_id
  LEFT JOIN forum_post ON forum_post.post_id = forum_forum.forum_last_post_id
  LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id
  LEFT JOIN forum_membres ON forum_membres.membre_id = forum_post.post_createur
  WHERE auth_view <= :lvl
  ORDER BY cat_ordre DESC, forum_ordre DESC');
  $query->bindValue(':lvl',$lvl,PDO::PARAM_INT);
  $query->execute();
  ?>

  <table>

    <tbody>

    <?php

    //Début de la boucle
    while($data = $query->fetch())
    {
      //On affiche chaque catégorie
      if( $categorie != $data['cat_id'] )
      {
        //Si c'est une nouvelle catégorie on l'affiche (car il y a plusieurs fois l'id des catégories dans la table forum_forum)
        $categorie = $data['cat_id'];
        ?>
        <tr>
          <th></th>
          <th class="titre"><strong><?php echo $data['cat_nom']; ?>
          </strong></th>
          <th class="nombremessages"><strong>Sujets</strong></th>
          <th class="nombresujets"><strong>Messages</strong></th>
          <th class="derniermessage"><strong>Dernier message</strong></th>
        </tr>
      <?php }

      //Ici, on met le contenu de chaque catégorie
      // les forums en détail : description, nombre de réponses etc...

      echo'<tr>
      <td><img src="../images/40793.gif" alt="message" /></td>
      <td class="titre"><strong><a href="../forum/voirforum.php?f='.$data['forum_id'].'">'.$data['forum_name'].'</a></strong><br/>
      '.$data['forum_desc'].'</td>
      <td class="nombresujets">'.$data['forum_topic'].'</td>
      <td class="nombremessages">'.$data['forum_post'].'</td>';

      // Deux cas possibles :
      // Soit il y a un nouveau message, soit le forum est vide
      if (!empty($data['forum_post']))
      {
        //Selection dernier message
        $nombreDeMessagesParPage = 15;
        $nbr_post = $data['topic_post'] +1;
        $page = ceil($nbr_post / $nombreDeMessagesParPage);

        echo'<td class="derniermessage">
        <div class="derniermessage-content">
        Par &nbsp <a href="../profil/voirprofil.php?m='.$data['membre_id'].'&amp;action=consulter"> ' .$data['membre_pseudo'].'</a>
        <a href="../forum/voirtopic.php?t='.$data['topic_id'].'&amp;page='.$page.'#p_'.$data['post_id'].'">
        <img src="../images/40792.gif" alt="new" /></a>
        </div>
        A '.date('H:i \l\e d M Y',$data['post_time']).'
        </td>
        </tr>';
      }
      else
      {
        echo'<td class="nombremessages">Pas de messages</td>
        </tr>';
      }

      //Cette variable stock le nombre de messages, on la met à jour
      $totaldesmessages += $data['forum_post'];

    } //fin de la boucle
    $query->CloseCursor();
    echo '</tbody></table></div></div></main>';

    //Le pied de page ici :
    echo'<div class="footer">

    <h2>Qui est en ligne ?</h2>';

    //On compte les membres
    $TotalDesMembres = $db->query('SELECT COUNT(*) FROM forum_membres')->fetchColumn();

    //On cherche le dernier membre inscrit
    $query = $db->query('SELECT membre_pseudo, membre_id FROM forum_membres ORDER BY membre_id DESC LIMIT 0, 1');
    $data = $query->fetch();
    $derniermembre = $data['membre_pseudo'];

    echo'<div><p>Le total des messages du forum est <strong>'.$totaldesmessages.'</strong>.</p>';
    echo'<p>Le site et le forum comptent <strong>'.$TotalDesMembres.'</strong> membres.</p>';
    echo'<p>Le dernier membre inscrit est <a href="../profil/voirprofil.php?m='.$data['membre_id'].'&amp;action=consulter">'.$derniermembre.'</a>.</p></div>';
    $query->CloseCursor();
    ?>

  </div>
</body>
</html>
